<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Cheap products</title>
</head>
<body>
<div class="content">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th> id</th>
            <th> name</th>
            <th> price</th>
            <th> image url</th>
            <th> rating</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cheapestProducts as $product)
            <tr>
                <td> {{$product->getId()}} </td>
                <td> {{$product->getName()}} </td>
                <td> {{$product->getPrice()}} </td>
                <td> {{$product->getImageUrl()}} </td>
                <td> {{$product->getRating()}} </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>