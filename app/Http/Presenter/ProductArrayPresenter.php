<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param  Product[]  $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $productCollection = [];
        foreach ($products as $product) {
            $productCollection[] = self::present($product);
        }
        return $productCollection;
    }

    public static function present(Product $product): array
    {
        $productPresentation = [];
        $productPresentation['id'] = $product->getId();
        $productPresentation['name'] = $product->getName();
        $productPresentation['price'] = $product->getPrice();
        $productPresentation['img'] = $product->getImageUrl();
        $productPresentation['rating'] = $product->getRating();
        return $productPresentation;
    }
}
