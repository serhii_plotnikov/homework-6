<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    public function showProducts(GetAllProductsAction $allProductsAction)
    {
        $products = $allProductsAction->execute()->getProducts();
        $productsCollection = ProductArrayPresenter::presentCollection($products);
        return response()->json($productsCollection);
    }

    public function showPopularProduct(GetMostPopularProductAction $mostPopularProductAction)
    {
        $popularProduct = $mostPopularProductAction->execute()->getProduct();
        $popularProduct = ProductArrayPresenter::present($popularProduct);
        return response()->json($popularProduct);
    }

    public function getCheapestProducts(GetCheapestProductsAction $cheapestProductsAction)
    {
        $cheapestProducts = $cheapestProductsAction->execute()->getProducts();
        return view('cheap_products', ['cheapestProducts' => $cheapestProducts]);
    }
}