<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetCheapestProductsResponse
{
    private $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}