<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetMostPopularProductResponse
{
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }
}