<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $productRepository;
    private const CHEAPEST_COUNT = 3;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        $productCollection = collect($this->productRepository->findAll());
        $sortedProductCollection = $productCollection->sortBy(function (Product $product) {
            return $product->getPrice();
        });
        return new GetCheapestProductsResponse($sortedProductCollection->slice(0, self::CHEAPEST_COUNT)
            ->values()->all());
    }
}