<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetMostPopularProductResponse
    {

        $productsCollection = collect($this->productRepository->findAll());
        $mostPopularProduct = $productsCollection->sortByDesc(function (Product $product) {
            return $product->getRating();
        });
        return new GetMostPopularProductResponse($mostPopularProduct->values()->shift());
    }
}